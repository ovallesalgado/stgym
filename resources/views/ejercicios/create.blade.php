@extends('layout.main')

@section('content')

@if(count($errors)>0)
<div class="alert alert-danger">

  <ul>
    @foreach($errors->all() as $error)
     <li>{{$error}}</li>
     @endforeach
  </ul>

</div>

@endif



<div class="container">
    <div class="">





    <div class="card text-white bg-dark mx-auto mt-5" style="max-width: 40rem;">
  <div class="card-header">
  <h4 class="card-title">Crear Ejercicio</h4>
  </div>
  <div class="card-body">
    

    <form class="form-horizontal" method="POST" action="{{ route('ejercicios.store') }}">
                        {{ csrf_field() }}
                        

                        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                            <label for="nombre" class=" control-label">Nombre</label>

                            <div class="">
                                <input id="nombre" type="text" class="form-control" name="nombre" value="{{old('nombre')}}" required autofocus>

                                @if ($errors->has('nombre'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('musculo') ? ' has-error' : '' }}">
                            <label for="musculo" class=" control-label">Musculo</label>

                            <div class="">
                                <input id="musculo" type="text" class="form-control" name="musculo" value="{{old('musculo') }}" required>

                                @if ($errors->has('musculo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('musculo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       


                        <div class="form-group{{ $errors->has('imagen') ? ' has-error' : '' }}">
                        <label for="imagen" class=" control-label">imagen</label>

                        <div class="">
                            <input id="imagen" type="text" class="form-control" name="imagen"  required>

                            @if ($errors->has('imagen'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('imagen') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>




                        
                        <div class="form-group">
                            <div class=" ">
                                <button type="submit" class="btn btn-primary">
                                    Guardar
                                </button>
                                <a class="btn btn-primary  js-scroll-trigger  " href="{{route('ejercicios.index')}}">Volver</a>
                            </div>
                        </div>
                    </form>
  </div>
</div>




    
    </div>
</div>
@endsection
