@extends('layout.main')

@section('title','Home')
@section('content')

@include('flash::message')


<div class="container ">

<div class="rounded float-right mt-2 mb-2">
<a href="{{route('ejercicios.create')}}" class="btn btn-dark">Crear Ejercicio</i></a>
</div>

<table class="table table-striped   table-responsive-md table-responsive-sm mx-auto mt-2">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Musculo</th>
      <th scope="col">Imagen</th>
      <th scope="col">Acciónes</th>

    </tr>
  </thead>
  <tbody>
  @foreach($ejercicios as $ejercicio)
    <tr>
      <th scope="row">{{$ejercicio->id}}</th>
      <td>{{$ejercicio->nombre}}</td>
      <td>{{$ejercicio->musculo}}</td>
      <td><img src="{{$ejercicio->imagen}}" style="height:100px; border-radius: 25px"alt=""></td>
  
      <td class="btn-group">
      
      
      <a href="{{route('ejercicios.show',$ejercicio->id)}}" class="btn btn-dark"><i class="fa fa-search" aria-hidden="true"></i></a>
      <a href="{{route('ejercicios.edit',$ejercicio->id)}}" class="btn btn-dark"><i class="fa fa-cogs" aria-hidden="true"></i></a>
      <a href="{{route('ejercicios.destroy',$ejercicio->id)}}" onclick="return confirm('¿Realmente desea eliminar el usuario?')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      
      
      </td>
    </tr>
   
@endforeach
  </tbody>
</table>
<div class="rounded float-right mt-2">
{!!$ejercicios->links()!!}
</div>


</div>
</div>

@endsection
