@extends('layout.main')

@section('title','Home')
@section('content')

@include('flash::message')


<div class="container ">

<div class="rounded float-right mt-2 mb-2">
<a href="{{route('rutinas.create')}}" class="btn btn-dark">Crear Rutina</a>
</div>
<table class="table table-responsive-sm table-responsive-md table-responsive-lg ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
        <th scope="col">Dia</th>
        <th scope="col">usuario ID</th>
        <th scope="col">Acciones</th>
      
    </tr>
  </thead>
  <tbody>
  @foreach($rutinas as $rutina)
  <tr>
      <th scope="row">{{ $rutina->id }}</th>
      <th>{{ $rutina->dia }}</th>
      <th>{{ $rutina->user_id }}</th>
    
     
      <th class="btn-group">
      
      <a href="{{route('rutinas.edit',$rutina->id)}}" class="btn btn-dark"><i class="fa fa-cogs" aria-hidden="true"></i></a>
      <a href="{{route('rutinas.destroy',$rutina->id)}}" onclick="return confirm('¿Realmente desea eliminar el usuario?')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      </th>
    </tr>
  @endforeach   
  </tbody>
</table>
<div class="rounded float-right">
{!!$rutinas->links()!!}
</div>
</div>


@endsection
{{--  hola  --}}