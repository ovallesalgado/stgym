@extends('layout.main')

@section('content')

@if(count($errors)>0)
<div class="alert alert-danger">

  <ul>
    @foreach($errors->all() as $error)
     <li>{{$error}}</li>
     @endforeach
  </ul>

</div>

@endif



<div class="container">
    <div class="">





    <div class="card text-white bg-dark mx-auto mt-5" style="max-width: 40rem;">
  <div class="card-header">
  <h4 class="card-title">Editar Rutina</h4>
  </div>
  <div class="card-body">
    

    <form class="form-horizontal" method="POST" action="{{ route('rutinas.update',['rutinas'=>$rutinas->id]) }}">
                        {{ csrf_field() }}
                        {{method_field('PUT')}}

                        <div class="form-group{{ $errors->has('dia') ? ' has-error' : '' }}">
                            <label for="dia" class=" control-label">Dia</label>

                            <div class="">
       
                                <select  id="dia" class="form-control" name="dia" value="{{$rutina->dia or old('dia')}}" required autofocus>
                                  <option value="lunes">Lunes</option>
                                  <option value="martes">Martes</option>
                                  <option value="miercoles">Miercoles</option>
                                  <option value="jueves">Jueves</option>
                                  <option value="viernes">Viernes</option>
                                  <option value="sabado">Sabado/option>
                                  <option value="domingo">Domingo/option>
                                  </select>

                                   


                                @if ($errors->has('dia'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dia') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      
                      
                      
                      
                        <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                            <label for="user_id" class=" control-label">user_id</label>

                            <div class="">
                                
                                   <select  id="user_id"  class="form-control" name="user_id" value="{{old('user_id')}}" required autofocus>
                                    @foreach($users as $user)
                                    
                                     <option value="{{$user->id}}">{{$user->name}} {{$user->apellido}}</option>
                                     @endforeach
                                  </select> 

                                @if ($errors->has('user_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       
                                    
   
 


                        




                        
                        <div class="form-group">
                            <div class=" ">
                                <button type="submit" class="btn btn-primary">
                                    Guardar
                                </button>
                                <a class="btn btn-primary  js-scroll-trigger  " href="{{route('rutinas.index')}}">Volver</a>
                            </div>
                        </div>
                    </form>
  </div>
</div>



 
    </div>
</div>
@endsection
