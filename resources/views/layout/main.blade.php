<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>  
     
   <link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">
   <link rel="stylesheet" href="{{asset('plugins/fontAwesome/css/font-awesome.min.css')}}">
   <link rel="stylesheet" href="{{asset('css/animate.css')}}">
   <link rel="stylesheet" href="{{asset('css/estilos.css')}}">


   <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'> 

    <!-- Plugin CSS -->
   <link href="{{asset('css/magnific-popup.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
   <link href="{{asset('css/creative.min.css')}}" rel="stylesheet">

   <body id="page-top">
   @include('layout.nav')

    

   <div class="container">
   
   </div>
    @yield('content')



   
  
  <script src="{{asset('plugins/jquery/jquery-3.2.1.min.js')}}"></script>
  <script src="{{asset('plugins/bootstrap/js/popper.min.js')}}"></script>
  <script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- Plugin JavaScript -->
    <script src="{{asset('plugins/jquery-easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('plugins/scrollreveal/scrollreveal.min.js')}}"></script>
    <script src="{{asset('plugins/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

<!-- Custom scripts for this template -->
<script src="{{asset('js/creative.min.js')}}"></script>
</body>
</html>