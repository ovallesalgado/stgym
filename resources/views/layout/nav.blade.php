
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark " id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#"><strong>Star</strong>Gym</a> 
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">


          <ul class="navbar-nav  mx-auto">
        @if (Auth::guest())
        @else
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('home') }}">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
            </li>
            {{--  <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li>
          --}}




@if(Auth::user() and Auth::user()->type=='admin')


<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  Admin
</a>
<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
  <a class="dropdown-item" href="{{route('users.index')}}">Usuarios</a>
  <a class="dropdown-item" href="{{route('rutinas.index')}}">Rutinas</a>
  <a class="dropdown-item" href="{{route('ejercicios.index')}}">Ejercicios</a>
  
</div>
</li>

@endif
      @endguest

          </ul>


   
          <ul class="navbar-nav   float-right">
            <!-- Authentication Links -->
            @if (Auth::guest())
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a></li>
                <li class="nav-item"><a  class="nav-link js-scroll-trigger"href="{{ route('register') }}">Register</a></li>
            @else
                <li class="dropdown nav-item">
                    <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink1" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                   {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                         
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink1">
                    <a class="dropdown-item" href="#">
                                Perfil
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        
                    
                    </div>
            @endguest
        </ul>

        </div>
      </div>
    </nav>