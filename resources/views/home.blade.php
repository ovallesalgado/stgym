@extends('layout.main')

@section('title','Home')
@section('content')
@include('layout.header')
@include('flash::message')




<section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">¡Tenemos lo que necesitas!</h2>
            <hr class="light">
            <p class="text-faded">Una formula ganadora es la habilidad de aceptar que hay una gran área de potencial sin explotar más allá de lo que actualmente percibes como tu máximo potencial!</p>
            <a class="btn btn-default btn-xl js-scroll-trigger" href="#services">Inicia!</a>
            

            
          </div>
        </div>
      </div>
    </section>

    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">A tu servicio</h2>
            <hr class="primary">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row ">
         
          
         
          <div class="col-lg-3 col-md-6 text-center mx-auto">
            <div class="service-box">
              <i class="fa fa-4x fa-heart text-primary sr-icons"></i>
              <h3>Rutinas</h3>
              <p class="text-muted">Tienes que hacer tus deberes con amor en todos los días!</p>
            </div>
          </div>
        </div>
      </div>
    </section>

  @include('layout.portafolio')

    {{--  <div class="call-to-action bg-dark">
      <div class="container text-center">
        <h2>Free Download at Start Bootstrap!</h2>
        <a class="btn btn-default btn-xl sr-button" href="http://startbootstrap.com/template-overviews/creative/">Download Now!</a>
      </div>
    </div>

    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Let's Get In Touch!</h2>
            <hr class="primary">
            <p>Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 ml-auto text-center">
            <i class="fa fa-phone fa-3x sr-contact"></i>
            <p>123-456-6789</p>
          </div>
          <div class="col-lg-4 mr-auto text-center">
            <i class="fa fa-envelope-o fa-3x sr-contact"></i>
            <p>
              <a href="mailto:your-email@your-domain.com">feedback@startbootstrap.com</a>
            </p>
          </div>
        </div>
      </div>
    </section>  --}}
 
<div class="mb-2"></div>
@endsection
