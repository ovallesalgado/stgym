@extends('layout.main')

@section('title','Home')
@section('content')

@include('flash::message')





<div class="card text-white text-center bg-dark mb-3 mt-3 mx-auto" style="max-width: 50rem;">
  <div class="card-header text-capitalize"><strong>{{ $user->name }} {{ $user->apellido }}</strong></div>
  <div class="card-body">
    <p class="card-text">Email:{{ $user->email }}</p>
    <p class="card-text text-capitalize text-center">Tipo: {{ $user->type}}</p>
    <hr>
    <a class="btn btn-primary  js-scroll-trigger  " href="{{route('users.index')}}">Volver</a>

  </div>
</div>


@endsection
