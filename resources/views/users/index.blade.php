@extends('layout.main')

@section('title','Home')
@section('content')

@include('flash::message')


<div class="container ">

<table class="table table-striped   table-responsive-md table-responsive-sm mx-auto mt-2">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Email</th>
      <th scope="col">Tipo</th>
      <th scope="col">Acciónes</th>

    </tr>
  </thead>
  <tbody>
  @foreach($users as $user)
    <tr>
      <th scope="row">{{$user->id}}</th>
      <td>{{$user->name}}</td>
      <td>{{$user->apellido}}</td>
      <td>{{$user->email}}</td>
      <td>
      @if($user->type=="admin")
      <span class="badge badge-pill badge-dark">{{ $user->type}}</span>
      @else
      <span class="badge badge-pill badge-info">{{ $user->type}}</span>
      @endif
      </td>
      <td class="btn-group">
      
      
      <a href="{{route('users.show',$user->id)}}" class="btn btn-dark"><i class="fa fa-search" aria-hidden="true"></i></a>
      <a href="{{route('users.edit',$user->id)}}" class="btn btn-dark"><i class="fa fa-cogs" aria-hidden="true"></i></a>
      <a href="{{route('users.destroy',$user->id)}}" onclick="return confirm('¿Realmente desea eliminar el usuario?')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      
      
      </td>
    </tr>
   
@endforeach
  </tbody>
</table>
<div class="rounded float-right mt-2">
{!!$users->links()!!}
</div>


</div>
</div>

@endsection
