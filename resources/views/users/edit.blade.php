@extends('layout.main')

@section('content')

@if(count($errors)>0)
<div class="alert alert-danger">

  <ul>
    @foreach($errors->all() as $error)
     <li>{{$error}}</li>
     @endforeach
  </ul>

</div>

@endif



<div class="container">
    <div class="">





    <div class="card text-white bg-dark mx-auto mt-5" style="max-width: 40rem;">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h4 class="card-title">Dark card title</h4>

    <form class="form-horizontal" method="POST" action="{{ route('users.update',['user'=>$user->id]) }}">
                        {{ csrf_field() }}
                        {{method_field('PUT')}}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class=" control-label">Nombre</label>

                            <div class="">
                                <input id="name" type="text" class="form-control" name="name" value="{{  $user->name or old('name')}}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                            <label for="apellido" class=" control-label">Apellido</label>

                            <div class="">
                                <input id="apellido" type="text" class="form-control" name="apellido" value="{{  $user->apellido or old('apellido')}}" required autofocus>

                                @if ($errors->has('apellido'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('apellido') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class=" control-label">E-Mail</label>

                            <div class="">
                                <input id="email" type="email" class="form-control" name="email" value="{{$user->email or old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       


                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class=" control-label">Tipo</label>

                          
                               
                                <select id="type" class="form-control" name="type" required autofocus>
                              
                                  <option value="member">Usuario</option>
                                  <option value="admin">Administrador</option>
                                 
                                </select>

                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            
                        </div>




                        
                        <div class="form-group">
                            <div class=" ">
                                <button type="submit" class="btn btn-primary">
                                    Guardar
                                </button>
                                <a class="btn btn-primary  js-scroll-trigger  " href="{{route('users.index')}}">Volver</a>
                            </div>
                        </div>
                    </form>
  </div>
</div>




    
    </div>
</div>
@endsection
