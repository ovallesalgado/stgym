@extends('layout.main')

@section('title','Home')
@section('content')

@include('flash::message')


<div class="container ">

<table class="table table-striped   table-responsive-md table-responsive-sm mx-auto mt-2">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">nombre</th>
      <th scope="col">Documento</th>
      <th scope="col">Apellido</th>
      <th scope="col">Telefono</th>
      <th scope="col">Genero</th>
      <th scope="col">cumpleaños</th>
      <th scope="col">interes</th>

    </tr>
  </thead>
  <tbody>

 
  

  @foreach($perfiles as $perfil)
    <tr>
      <th scope="row">{{$perfil->user_id}}</th>
      
      <td>{{$perfil->documento}}</td>
      <td>{{$perfil->apellido}}</td>
      <td>{{$perfil->telefono}}</td>
      <td>{{$perfil->genero}}</td>
      <td>{{$perfil->cumpleanios}}</td>
      <td>{{$perfil->interes}}</td>
      
      </td>
      <td class="btn-group">
      
      <a href="#" class="btn btn-dark"><i class="fa fa-search" aria-hidden="true"></i></a>
      <a href="#" class="btn btn-dark"><i class="fa fa-cogs" aria-hidden="true"></i></a>
      <a href="#" onclick="return confirm('¿Realmente desea eliminar el usuario?')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
     
      </td>
    </tr>
   
@endforeach

  </tbody>
</table>
<div class="rounded float-right mt-2">

</div>


</div>
</div>

@endsection
