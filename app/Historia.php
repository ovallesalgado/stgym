<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historia extends Model
{
    protected $table="historias";
    
    protected $fillable = [
        'id',
        'estatura',
        'peso',
        'tCuello',
        'tBrazo',
        'tAntebrazo',
        'tDorsal',
        'tPectoral',
        'tAbdominal',
        'tCintura',
        'tCadera',
        'tPierna',
        'tPantorilla',
        'user_id',

    ];

    public function users(){
        return $this->belongsTo('App\User');
    }
}
