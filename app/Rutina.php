<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rutina extends Model
{
    protected $table="rutinas";
    
    protected $fillable = [
        'id',
        'dia',
        'user_id',
    ];

    
    public function users(){
        return $this->belongsTo('App\User');
    }

    public function rutina_ejercicio(){
        return $this->hasMany('App\rutina_ejercicio');
    }


}
