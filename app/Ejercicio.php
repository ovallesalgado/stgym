<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ejercicio extends Model
{
    protected $table="ejercicios";
    
    protected $fillable = [
        'id',
        'nombre',
        'musculo',
        'imagen',
    ];

    public function rutina_ejercicio(){
        return $this->hasMany('App\rutina_ejercicio');
    }
}
