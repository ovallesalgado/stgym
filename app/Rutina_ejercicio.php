<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rutina_ejercicio extends Model
{
    protected $table="rutina_ejercicio";
    
    protected $fillable = [
        'id',
        'rutina_id',
        'user_id',
        'series',
        'repeticiones',

    ];

    public function rutina(){
        return $this->belongsTo('App\Rutina');
    }

    public function ejercicio(){
        return $this->belongsTo('App\Ejercicio');
    }
}
