<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ejercicio;

class EjerciciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ejercicios=Ejercicio::orderBy('id', 'asc')->paginate(10);
        
                return view('ejercicios.index')->with(['ejercicios'=> $ejercicios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ejercicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $ejercicio = new Ejercicio($request->all());
           
        $ejercicio->save();
        flash("Se ha registrado el usuario de forma exitosa")->success()->important();
        
        return redirect()->route('ejercicios.index');
    

  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ejercicios=Ejercicio :: find($id);
        
        return view('ejercicios.edit')->with('ejercicio', $ejercicios);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ejercicios=Ejercicio::find($id);
        $ejercicios->fill($request->all());
        $ejercicios->save();
        flash("Se ha editado el ejercicio de forma exitosa")->success()->important();
        return redirect()->route('ejercicios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ejercicios=Ejercicio:: find($id);
        $ejercicios->delete();
        flash("Se ha eliminado el ejercicio de forma exitosa")->error()->important();
        return redirect()->route('ejercicios.index');
    }
}
