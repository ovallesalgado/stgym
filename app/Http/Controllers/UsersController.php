<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Rutina;
use App\Ejercicios_usuario;
use DB;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users=User::orderBy('id', 'asc')->paginate(10);

        return view('users.index')->with(['users'=> $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd('Hola desde controller create user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::find($id);
        
          return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user=User::find($id);
        $user->fill($request->all());
        $user->save();
        flash("Se ha editado el usuario de forma exitosa")->success()->important();
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User:: find($id);
        $user->delete();
        flash("Se ha eliminado el usuario de forma exitosa")->error()->important();
        return redirect()->route('users.index');
    }


    public function lista(){

       $Ejercicios_usuario=Ejercicios_usuario::all();
       $Ejercicios_usuario->user_id = \Auth::user()->id;
       dd($Ejercicios_usuario);
        
       return view('users.index')->with(['users'=> $users]);    
    }
}
