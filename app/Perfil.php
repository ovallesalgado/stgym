<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table="perfiles";
    
    protected $fillable = [
        'user_id',
        'documento',
        'apellido',
        'telefono',
        'genero',
        'cumpleanios',
        'interes',

    ];

    public function users(){
        return $this->hasOne('App\User');
    }

 
}
