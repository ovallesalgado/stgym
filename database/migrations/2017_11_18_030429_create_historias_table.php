<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historias', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('estatura', 5, 2);
            $table->decimal('peso', 5, 2);
            $table->decimal('tCuello', 5, 2);
            $table->decimal('tBrazo', 5, 2);
            $table->decimal('tAntebrazo', 5, 2);
            $table->decimal('tDorsal', 5, 2);
            $table->decimal('tPectoral', 5, 2);
            $table->decimal('tAbdominal', 5, 2);
            $table->decimal('tCintura', 5, 2);
            $table->decimal('tCadera', 5, 2);
            $table->decimal('tPierna', 5, 2);
            $table->decimal('tPantorilla', 5, 2);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historias');
    }
}
