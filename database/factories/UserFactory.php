<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'apellido' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Historia::class, function (Faker $faker) {
 

    return [
        'estatura' =>$faker->randomFloat(2, 1, 100 ), 
        'peso' =>$faker->randomFloat(2, 1, 100 ), 
        'tCuello' =>$faker->randomFloat(2, 1, 100 ), 
        'tBrazo' =>$faker->randomFloat(2, 1, 100 ), 
        'tAntebrazo' =>$faker->randomFloat(2, 1, 100 ), 
        'tDorsal' =>$faker->randomFloat(2, 1, 100 ), 
        'tPectoral' =>$faker->randomFloat(2, 1, 100 ), 
        'tAbdominal' =>$faker->randomFloat(2, 1, 100 ), 
        'tCintura' =>$faker->randomFloat(2, 1, 100 ), 
        'tCadera' =>$faker->randomFloat(2, 1, 100 ), 
        'tPierna' =>$faker->randomFloat(2, 1, 100 ), 
        'tPantorilla' =>$faker->randomFloat(2, 1, 100 ), 
        'user_id' =>$faker->randomDigitNotNull, 
        
    ];
});

$factory->define(App\Rutina::class, function (Faker $faker) {


    return [
        'dia' => $faker->dayOfWeek($max = 'now'),
        'user_id' =>$faker->randomDigitNotNull, 
    ];
});


$factory->define(App\Ejercicio::class, function (Faker $faker) {

    return [
        'nombre' => $faker->firstName,
        'musculo' =>$faker->firstName, 
        'imagen' =>$faker->imageUrl($width=200, $height=200, 'people'), 
    ];
});

$factory->define(App\Rutina_ejercicio::class, function (Faker $faker) {
    
        return [
            'rutina_id' => $faker->randomDigitNotNull, 
            'ejercicio_id' =>$faker->randomDigitNotNull,  
            'series' =>$faker->randomDigitNotNull, 
            'repeticiones' =>$faker->randomDigitNotNull,  
        ];
    });


    $factory->define(App\Perfil::class, function (Faker $faker) {
        
            return [
                'user_id' =>$faker->randomDigitNotNull,  
                'documento' => $faker->randomNumber($nbDigits = NULL, $strict = false), 
                'telefono' =>$faker->e164PhoneNumber,
                'genero' => $faker->randomElement(['Masculino', 'Femenino']),
                'cumpleanios' => $faker->date,
                'interes' => $faker->text,  
            ];
        });