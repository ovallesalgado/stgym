<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/hola', 'UsersController@lista')->name('lista');



Route::group(['prefix'=>'admin'],function(){

Route::resource('users','UsersController');
Route:: get ('users/{id}/destroy',[
    'uses'=>'UsersController@destroy',
    'as'=>'users.destroy',
]);

Route::resource('perfiles','PerfilesController');
Route:: get ('perfiles/{id}/destroy',[
    'uses'=>'PerfilesController@destroy',
    'as'=>'perfiles.destroy',
]);

Route:: resource ('rutinas','RutinasController');
Route:: get ('rutinas/{id}/destroy',[
    'uses'=>'RutinasController@destroy',
    'as'=>'rutinas.destroy'
]);

Route::resource('ejercicios','EjerciciosController');
Route:: get ('ejercicios/{id}/destroy',[
    'uses'=>'EjerciciosController@destroy',
    'as'=>'ejercicios.destroy',
]);


});